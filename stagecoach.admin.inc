<?php


function stagecoach_settings_form() {
  $form = array();
  
  $types = node_get_types();
  $options = array('0' => '');
  foreach ($types as $code_name => $info) {
    $options[$code_name] = $info->name;
  }

  $form['stagecoach_content_types'] = array(
    '#type' => 'select',
    '#title' => t('Content types to able to synchronize'),
    '#default_value' => variable_get('stagecoach_content_types', '0'),
    '#options' => $options,
    '#multiple' => TRUE,
  );
  
  $default_url = 'http://stage-' . $_SERVER['HTTP_HOST'] . base_path();
  $form['stagecoach_stage_baseurl'] = array(
    '#type' => 'textfield',
    '#title' => t('Base URL for stage server'),
    '#default_value' => variable_get('stagecoach_stage_baseurl', $default_url),
    '#required' => TRUE,
  );

  if (stagecoach_check_db()) {
    $form['stagecoach_stage_comments'] = array(
      '#type' => 'fieldset',
      '#title' => t('Commenting'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    
    $form['stagecoach_stage_comments']['stagecoach_stage_comments_stage'] = array(
      '#type' => 'checkbox',
      '#title' => t('Commenting enabled on stage'),
      '#default_value' => variable_get('stagecoach_stage_comments_stage', COMMENT_NODE_READ_WRITE),
      '#return_value' => COMMENT_NODE_READ_WRITE,
    );
    
    if (variable_get('stagecoach_stage_comments_stage', COMMENT_NODE_READ_WRITE)) {
      $form['stagecoach_stage_comments']['stagecoach_stage_comments_prod'] = array(
        '#type' => 'checkbox',
        '#title' => t('Commenting enabled on production'),
        '#default_value' => variable_get('stagecoach_stage_comments_prod', COMMENT_NODE_DISABLED),
        '#return_value' => COMMENT_NODE_READ_WRITE,
      );
    }
    
  }
  
  return system_settings_form($form);
}


function stagecoach_settings_form_validate($form, &$form_state) {
  $baseurl = $form_state['values']['stagecoach_stage_baseurl'];
  $response = drupal_http_request($baseurl);
  if ($response->code != 200) {
    form_set_error('', t('The given base URL (!link) does not seem to work.', array('!link' => $baseurl)));
  }
  
  $content_types = array_keys($form_state['values']['stagecoach_content_types']);
  $commenting = $form_state['values']['stagecoach_stage_comments_stage'];
  db_set_active('stage');
  foreach ($content_types as $c) {
    db_query("UPDATE {node} SET comment = %d WHERE type = '%s'", $commenting, $c);
  }
  db_set_active('default');
}
