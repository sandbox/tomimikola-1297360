= Installation =
1. Define stage site in production site's settings.php file: 
    $db_url['default'] = 'mysqli://<prod_db_user>:<password>@<prod_db_server>/<prod_db_name>';
    $db_url['stage'] = 'mysqli://<stage_db_user>:<password>@<stage_db_server>/<stage_db_name>';
2. Enable module
3. Configure settings on admin/content/stagecoach/settings page

= Running synchronization =
1. Go to page admin/content/stagecoach/run
2. Choose options and press 'Sync content' button
3. Wait for selected content to be synced from stage to production
