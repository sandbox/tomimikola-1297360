<?php

/**
 * Implementation of hook_init().
 */
function stagecoach_init() {
  drupal_add_css( drupal_get_path('module', 'stagecoach') . '/stagecoach.css');
  drupal_add_js( drupal_get_path('module', 'stagecoach') . '/stagecoach.js');
}


/**
 * Implements hook_menu()
 */
function stagecoach_menu() {
  $items = array();

  $items['admin/content/stagecoach'] = array(
    'title' => t('Content synchronization'),
    'description' => t('Content staging and publishing on production site'),
    'page callback' => 'stagecoach_sync_history_page',
    'file' => 'stagecoach.inc',
    'access arguments' => array('publish staged content'),
    'weight' => 10,
    'type' => MENU_NORMAL_ITEM,
  );
  $items['admin/content/stagecoach/settings'] = array(
    'title' => t('Settings'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('stagecoach_settings_form'),
    'file' => 'stagecoach.admin.inc',
    'access arguments' => array('administer site configuration'),
    'weight' => 10,
    'type' => MENU_LOCAL_TASK,
  );
  $items['admin/content/stagecoach/history'] = array(
    'title' => t('Event log'),
    'page callback' => 'stagecoach_sync_history_page',
    'file' => 'stagecoach.inc',
    'access arguments' => array('publish staged content'),
    'weight' => 0,
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );
  $items['admin/content/stagecoach/run'] = array(
    'title' => t('Retrieve new content'),
    'description' => t('Syncronize content from staging server.'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('stagecoach_sync_run_form'),
    'file' => 'stagecoach.inc',
    'access arguments' => array('publish staged content'),
    'weight' => 2,
    'type' => MENU_LOCAL_TASK,
  );
  $items['admin/content/stagecoach/changed_content'] = array(
    'title' => t('Changes'),
    'page callback' => 'stagecoach_changed_content_page',
    'file' => 'stagecoach.inc',
    'access arguments' => array('publish staged content'),
    'weight' => 1,
    'type' => MENU_LOCAL_TASK,
  );
  $items['stagecoach/revisionlog/%node'] = array(
    'title' => t('Revision log'),
    'page callback' => 'stagecoach_node_changelog_ajax',
    'page arguments' => array(2),
    'file' => 'stagecoach.inc',
    'access arguments' => array('publish staged content'),
    'type' => MENU_CALLBACK,
  );
  
  return $items;
}


/**
 * Implementation of hook_perm
 */
function stagecoach_perm() {
  return array(
    'publish staged content',
    'modify staged content',
  );
}

function stagecoach_check_db() {
  global $db_url;
  $stage_db_found = (is_array($db_url) && isset($db_url['stage']));

  if (!$stage_db_found) {
    drupal_set_message(t('"stage" database settings cannot be found in the settings.php. You should not enable this module on staging sites but on production.'), 'warning');
  }
  
  return $stage_db_found;
}
