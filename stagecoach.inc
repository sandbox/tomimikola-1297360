<?php

function stagecoach_sync_history_page() {
  stagecoach_check_db();
  return stagecoach_sync_history_table();
}

function stagecoach_changed_content_page($timestamp_since = 0) {
  
  if (!$timestamp_since) {
    $timestamp_since = time() - 60*60*24*31;
    drupal_goto('admin/content/stagecoach/changed_content/' . $timestamp_since);
  }
  
  $title = t('Changes: %from - %to', array(
                '%from' => date('j.n.Y H:i', $timestamp_since),
                '%to' => date('j.n.Y H:i', time()
  )));
  drupal_set_title($title);
  
  stagecoach_check_db();
  return stagecoach_changed_node_table($timestamp_since);
}

function stagecoach_sync_run_form(&$form_state) {

  stagecoach_check_db();
  
  $selected_types = variable_get('stagecoach_content_types', '0');
  $node_info = node_get_types();
  
  $options = array('all' => t('All'));
  foreach ($selected_types as $code_name) {
    $options[$code_name] = $node_info[$code_name]->name;
  }

  $form['sync_nodes_by_type'] = array(
    '#type' => 'select',
    '#title' => t('Sync all nodes by type'),
    '#default_value' => 'all',
    '#options' => $options,
  );
  
  
  $langs = language_list();
  $options = array('all' => t('All'));
  foreach ($langs as $l => $lang) {
    $options[$l] = $lang->name;
  }
  $form['sync_lang'] = array(
    '#type' => 'select',
    '#title' => t('Filter by language'),
    '#default_value' => '0',
    '#options' => $options,
  );
  
  $default = strtotime('-1 year');
  $form['sync_modified_since'] = array(
    '#type' => 'date',
    '#title' => t('Sync only content modified since'),
    '#default_value' => array('year' => date('Y', $default), 'month' => date('n', $default), 'day' => date('j', $default)),
  );
  
  $form['submit'] = array('#type' => 'submit', '#value' => t('Sync content') );
  return $form;
}


function stagecoach_sync_run_form_submit($form_id, &$form_state) {

  $lock = variable_get('stagecoach_sync_running', 0);
  if ($lock > 0) {
    drupal_set_message(t('Sync process seems to be running. Wait for a few minutes.'));
    return;
  }
  //variable_set('stagecoach_sync_running', 1);
  
  $type = check_plain($form_state['values']['sync_nodes_by_type']);
  $lang = check_plain($form_state['values']['sync_lang']);
  $date = $form_state['values']['sync_modified_since'];
  $time = mktime(0, 0, 0, $date['month'], $date['day'], $date['year']);
  $now = time();
  db_set_active('stage');
  
  $type_sql = ($type == 'all')? '1=1' : "type = '$type'";
  if ($lang == 'all') {
    $result = db_query("SELECT nid FROM {node} WHERE $type_sql AND changed >= %d", $type, $time);
  }
  else {
    $result = db_query("SELECT nid FROM {node} WHERE $type_sql AND language = '%s' AND changed >= %d", $type, $lang, $time);
  }

  while ($nid = db_result($result)) {
    $nids[] = $nid;
  }
  unset($result);
  db_set_active('default');

  $synced_names = array();
  $updated_count = $count_new = 0;
  foreach ($nids as $nid) {
    db_set_active('stage');
    $node = node_load($nid);
    $stage_timestamp = db_result(db_query("SELECT changed FROM {node} WHERE nid = %d", $nid));
    db_set_active('default');
    
    $prod_timestamp = db_result(db_query("SELECT changed FROM {node} WHERE nid = %d", $nid));
    if ($stage_timestamp == $prod_timestamp) {
      continue;
    }
    
    stagecoach_backup_content_view($nid);
    
    $result = db_query("SELECT title FROM {node} WHERE nid = %d", $node->nid);
    //Create new node when necessary.
    if (db_result($result) != $node->title) {
      unset($node->nid);
      $count_new++;
      //@todo: watchdog message here
    }
    $node->revision = TRUE; //Create new revision. Otherwise Drupal confuses with versioning and returns 404 on node page.
    node_save($node);
    $updated_count++;
    $synced_names[] = check_plain($node->title);
  }

  $msg = t("%count nodes of type %type has been synchronized. %count_new new nodes created.<br/>
            !synced",
              array('%type' => $type, '%count' => $updated_count, '%count_new' => $count_new,
                    '!synced' => join('<br/>', $synced_names)));
  drupal_set_message($msg);
  
  //Change settings when commenting is enabled on stage but disabled on production (comment settings are copied from stage)
  if (variable_get('stagecoach_stage_comments_prod', COMMENT_NODE_DISABLED) !== variable_get('stagecoach_stage_comments_stage', COMMENT_NODE_READ_WRITE)) {
    db_query("UPDATE {node} SET comment = %d WHERE type = '%s'", variable_get('stagecoach_stage_comments_prod', COMMENT_NODE_DISABLED), $type);
  }
  
  db_query("INSERT INTO {stagecoach_sync_log} (timestamp, comment, type, language) VALUES (%d, '%s', '%s', '%s')", $now, $msg, $type, $lang);
  variable_set('stagecoach_sync_running', 0);

}


function stagecoach_changed_node_table($timestamp_since) {
  $table_rows = array();
  
  $sync_log = stagecoach_sync_log();

  db_set_active('stage');
  $result = db_query("SELECT title, type, nid, language, changed FROM {node} WHERE changed >= %d ORDER BY changed DESC", $timestamp_since);
  db_set_active('default');
  
  $log = array_shift($sync_log);
  $count = 0;
  while ($row = db_fetch_object($result)) {
    if ($log && $log['timestamp'] > $row->changed) {
      //Sync operation has been executed after the following content item changed
      //Print message row to the table
      $table_row = array('data' =>
                            array('data' => t('Sync operation (%time)<br/>type:%type, language:%lang<br/>!msg', array(
                                                    '%time' => $log['time_formatted'],
                                                    '!msg' => $log['comment'],
                                                    '%type' => $log['type'],
                                                    '%lang' => $log['language'],
                                              )),
                                  'colspan' => 4),
                        );
      $table_rows[] = array('data' => $table_row, 'class' => 'message');
      $log = array_shift($sync_log);
    }
    $link = '';
    if (variable_get('stagecoach_stage_baseurl', FALSE)) {
      $url = variable_get('stagecoach_stage_baseurl', FALSE) . 'node/' . $row->nid;
      $link = "<a href='$url' target='_blank'>$url</a>";
    }
    $title = "<a onclick='contentStagingLog({$row->nid}, \"{$timestamp_since}\", this); return false;'>" . $row->title . '</a>';
    $table_rows[] = array($title, $row->type, $row->language, $link);
    $count++;
  }
  
  if ($count < 1) {
    return t('No changes on stage.');
  }
    
  return theme('table', array(t('Title'), t('Type'), t('Language'), t('URL')), $table_rows, array('class' => 'stagecoach_change_history'));
}


function stagecoach_sync_log() {
  $result = db_query("SELECT * FROM {stagecoach_sync_log} ORDER BY timestamp DESC");
  
  $items = array();
  while ($row = db_fetch_object($result)) {
    $items[] = array(
                  'timestamp' => $row->timestamp,
                  'time_formatted' => date('d.m.Y G:i:s', $row->timestamp),
                  'comment' => $row->comment,
                  'type' => $row->type,
                  'language' => $row->language,
                );
  }
  
  return $items;
}


function stagecoach_sync_history_table() {
  $items = stagecoach_sync_log();
  $table_rows = array();
  foreach ($items as $i) {
    $table_rows[] = array(
                      l($i['time_formatted'], 'admin/content/stagecoach/changed_content/' . $i['timestamp']),
                      $i['comment'],
                      $i['type'],
                      $i['language']
                    );
  }

  return theme('table',
          array(t('Time'), t('Comments'), t('Type'), t('Language')),
          $table_rows,
          array('class' => 'stagecoach_sync_history')
        );
}

function stagecoach_last_sync_time() {
  return db_result(db_query("SELECT timestamp FROM {stagecoach_sync_log} ORDER BY timestamp DESC LIMIT 1"));
}


function stagecoach_node_changelog_ajax($node, $time_since = 0) {
  db_set_active('stage');
  $revisions = node_revision_list($node);
  $output = '<p><strong>' . t('Changes for %title:', array('%title' =>  $node->title)) . '</strong></p>';
  
  
  if (!empty($revisions)) {
    $vids = array_keys($revisions);
    
    for ($i=0; $i<count($vids); $i++) {
      $r = $revisions[$vids[$i]];
      $r_prev = $revisions[$vids[$i+1]];
      $author = user_load($r->uid)->name;
      
      if ($r->timestamp < $time_since) {
        break;
      }
      $output .= '<p>';
      $output .= '<strong>' . date('d.m.Y H:i', $r->timestamp) . " / $author " . check_plain($r->log) . "</strong><br/>";

      if (module_exists('diff')) {
        module_load_include('inc', 'diff', 'diff.pages');
        $new_node = node_load($node->nid, $r->vid);
        $old_node = node_load($node->nid, $r_prev->vid);
        $rows = _diff_body_rows($old_node, $new_node);
        $output .= theme('diff_table', array(), $rows, array('class' => 'diff'), NULL, array());
      }
      
      $output .= '</p>';
    }
    
  }
  
  db_set_active('default');
  echo $output;
  exit();
}


function stagecoach_backup_content_view($nid) {
  db_set_active('stage');
  $node = node_load($nid);
  $output = node_view($node, FALSE, TRUE);
  if (function_exists('comment_render') && $node->comment) {
    $output .= comment_render($node, NULL);
  }
  
  db_set_active('default');
  db_query("INSERT INTO {stagecoach_backups} (nid, timestamp, data) VALUES (%d, %d, '%s')", $nid, time(), $output);
}