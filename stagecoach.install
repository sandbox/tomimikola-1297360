<?php

function stagecoach_install() {
  drupal_install_schema('stagecoach');
  variable_set('stagecoach_content_types', '');
  variable_set('stagecoach_installation_time', time());
}

function stagecoach_uninstall() {
  drupal_uninstall_schema('stagecoach');
  variable_del('stagecoach_sync_running');
  variable_del('stagecoach_content_types');
  variable_del('stagecoach_installation_time');
}

/**
 * Implementation of hook_schema().
 */
function stagecoach_schema() {
  $schema['stagecoach_sync_log'] = array(
    'description' => 'Synchronization statistics.',
    'fields' => array(  
      'timestamp' => array(
        'description' => 'The time synchronization executed.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'type' => array(
        'description' => 'The type of the content items synchronized.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
        'default' => '',
      ),
      'language' => array(
        'description' => 'Language of the content items synchronized.',
        'type' => 'varchar',
        'length' => 12,
        'not null' => FALSE,
        'default' => '',
      ),
      'comment' => array(
        'description' => 'Comment field',
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
        'default' => '',
       ),
     ),
  );
  
  $schema['stagecoach_backups'] = array(
    'fields' => array(
      'nid' => array(
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'timestamp' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'data' => array(
        'type' => 'text',
        'not null' => FALSE,
        'size' => 'big',
        'default' => '',
      ),
     ),
     'indexes' => array(
        'nid_indx' => array('nid'),
      ),
  );

  return $schema;
}

function stagecoach_requirements($phase) {
  $requirements = array();
  
  if ($phase == 'runtime') {
    $ok = FALSE;
    global $db_url;
    if (is_array($db_url) && isset($db_url['stage'])) {
      db_set_active('stage');
      $version = db_version();
      db_set_active('default');
      $ok = ($version > 0);
    }
  
    
    $val = $ok? 'Configured' : t('Settings not found.');
    $desc = $ok? '' : t('You must define "stage" database settings in the settings.php. See module README.txt file.');
    
    $severity = $ok? REQUIREMENT_OK : REQUIREMENT_ERROR;
    $requirements['stagecoach'] = array(
      'title' => t('Stagecoach'), 
      'description' => $desc,
      'value' => $val,
      'severity' => $severity,
    );
  }
  
  return $requirements;
}