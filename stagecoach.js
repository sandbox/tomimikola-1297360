function contentStagingLog(nid, since, elem) {
  $tr = $(elem).closest('tr');
  if ($tr.hasClass('opened')) {
    $tr.next('tr.diff-message').remove();
    $tr.removeClass('opened');
    return false;
  }
  else {
    $tr.addClass('opened');
  }
  $.get(Drupal.settings.basePath + 'stagecoach/revisionlog/' + nid +'/' + since, function(data) {
  $msg = $('<tr class="diff-message"><td colspan="4">' + data + '</td></tr>');
  $msg.hide();
  $tr.after($msg);
  $msg.fadeIn();
});
}